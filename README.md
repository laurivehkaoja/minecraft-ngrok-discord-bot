# Minecraft Ngrok Discord Bot

This is a Discord bot made to help people sharing an Ngrok hosted Minecraft Server's IP to a Discord server. Application is written in Python and it uses the Ngrok API to communicate and contact to your Ngrok account.

# Setup

Clone the bot with `git clone https://gitlab.com/laurivehkaoja/linux-stuff.git`. 

Log in to Ngrok. Open API tab and create copy an API key. 

Log in to Discord Developer site https://discord.com/developers. Add a new Application. Generate bot token. Enable `Message Content Intent` option.

Navigate to cloned folder. Open `.env.example`. Insert bot token and Ngrok API key. Renamne `.env.example` to `.env`

# Running

The bot can be runned by executing it with python: `python bot.py`
