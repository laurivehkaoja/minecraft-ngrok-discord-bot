import discord
from dotenv import load_dotenv
from get_ip import get_ip
import os

ip = get_ip()

class MyClient(discord.Client):
    async def on_ready(self):
        print(f"Bot {self.user} is online!")

    async def on_message(self, message):
        if message.author == self.user:
            return
        if message.content == "ip":
            await message.channel.send(f"IP: `{ip}`")

load_dotenv()

if __name__ == "__main__":
    intents = discord.Intents.default()
    intents.message_content = True
    bot = MyClient(intents=intents)
    TOKEN = os.getenv("TOKEN")
    bot.run(TOKEN)
