# Imports
import os
import ngrok
from dotenv import load_dotenv

load_dotenv()

APIKEY = os.getenv("APIKEY")

client = ngrok.Client(APIKEY) # Connect to your Ngrok account via API key

def get_tunnels():
    for tunnel in client.tunnels.list():
        return tunnel

def get_ip():
    tunnel = get_tunnels()
    return tunnel.public_url[6:]

